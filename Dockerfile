FROM registry.sindominio.net/debian as builder

ARG WATCHTOWER_VERSION=main

RUN apt-get update && \
    apt-get install -y git tzdata wget build-essential

## Install Go 1.19 via Go web
RUN wget https://golang.org/dl/go1.19.1.linux-amd64.tar.gz && \
		echo "acc512fbab4f716a8f97a8b3fbaa9ddd39606a28be6c2515ef7c6c6311acffde go1.19.1.linux-amd64.tar.gz" > go.checksum && \
		sha256sum -c go.checksum && \
		rm -rf /usr/local/go && \
		rm -rf /usr/bin/go && \
		tar -C /usr/local -xzf go1.19.1.linux-amd64.tar.gz && \
		export PATH=/usr/local/go/bin:$PATH

RUN ln -s /usr/local/go/bin/go /usr/bin/go

## Watchtower
RUN git clone --branch "${WATCHTOWER_VERSION}" https://github.com/containrrr/watchtower.git

RUN \
  cd watchtower && \
  GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' . && \
  GO111MODULE=on go test ./... -v

FROM scratch

LABEL "com.centurylinklabs.watchtower"="true"

# copy files from other container
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /watchtower/watchtower /watchtower

ENTRYPOINT ["/watchtower"]
